<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Marker;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MarkerController extends Controller
{
    public function getAllMarker()
    {
        return response()->json(Marker::where('userId', Auth::user()->id)->get(), 200);
    }

    public function getMarkerById($id)
    {
        $map = Marker::find($id);
        if (is_null($map)) {
            return response()->json(['error' => true, 'message' => 'Not found'], 404);
        }
        return response()->json($map, 200);
    }

    public function addMarker(Request $req)
    {

        $req->validate([
            'name' => 'required',
            'longitude' => 'required',
            'latitude' => 'required',
        ]);

        $map = Marker::create([
            'userId' => Auth::user()->id,
            'name' => $req->name,
            'longitude' => $req->longitude,
            'latitude' => $req->latitude,
        ]);
        return response()->json($map, 201);
    }

    public function updateMarker(Request $req, $id)
    {
        $map = Marker::find($id);
        if (is_null($map)) {
            return response()->json(['error' => true, 'message' => 'Not found'], 404);
        }

        $req->validate([
            'name' => 'required',
            'longitude' => 'required',
            'latitude' => 'required',
        ]);

        $map->update($req->all());
        return response()->json($map, 200);
    }

    public function deleteMarker(Request $req, $id)
    {
        $map = Marker::find($id);
        if (is_null($map)) {
            return response()->json(['error' => true, 'message' => 'Not found'], 404);
        }
        $map->delete();
        return response()->json('', 204);
    }
}
