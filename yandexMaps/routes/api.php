<?php

use App\Http\Controllers\Api\MarkerController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


//Route::group(['middleware' => ['auth:api']], function () {
//    Route::get('marker', [MarkerController::class, 'getAllMarker']);
//    Route::get('marker/{id}', [MarkerController::class, 'getMarkerById']);
//    Route::post('marker', [MarkerController::class, 'addMarker']);
//    Route::put('marker/{id}', [MarkerController::class, 'updateMarker']);
//    Route::delete('marker/{id}', [MarkerController::class, 'deleteMarker']);
//});
//Marker

