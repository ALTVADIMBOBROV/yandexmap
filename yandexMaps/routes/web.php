<?php

use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\MarkerController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if(\Illuminate\Support\Facades\Auth::check()){
        return redirect()->route('home');
    }
    return view('welcome');
});

Auth::routes();

Route::get('/home', [ HomeController::class, 'index'])->name('home');


//Marker Route
Route::group(['middleware' => ['auth'],'prefix'=>'/api'], function () {
    Route::get('marker', [MarkerController::class, 'getAllMarker']);
    Route::get('marker/{id}', [MarkerController::class, 'getMarkerById']);
    Route::post('marker', [MarkerController::class, 'addMarker']);
    Route::put('marker/{id}', [MarkerController::class, 'updateMarker']);
    Route::delete('marker/{id}', [MarkerController::class, 'deleteMarker']);
});
